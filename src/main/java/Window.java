import lombok.Getter;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

class Window {
    @FunctionalInterface
    interface Renderer {
        void doRender() throws Exception;
    }

    @FunctionalInterface
    interface KeyListener {
        void handle(int key, int scancode, int action);
    }
@Getter
    private final long window;

    Window(int width, int height, String caption) {
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        // Создание окна
        window = glfwCreateWindow(width, height, caption, NULL, NULL);
        if (window == NULL)
            throw new RuntimeException("Ошибка создания GLFW окна");

        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        // Center our window
        glfwSetWindowPos(
            window,
            (vidmode.width() - width) / 2,
            (vidmode.height() - height) / 2
        );


        glfwMakeContextCurrent(window);
        glfwSwapInterval(1);
        glfwShowWindow(window);

        GL.createCapabilities();

        glfwSetWindowSizeCallback(window, new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int width, int height) {
                glViewport(0, 0, width, height);
            }
        });
      //  glEnable(GL_BLEND);
       // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    }


    void setKeyListener(KeyListener keyListener) {
        glfwSetKeyCallback(window, new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                    glfwSetWindowShouldClose(window, true); // We will detect this in our rendering render
                keyListener.handle(key, scancode, action);
            }
        });
    }

    void render(Renderer renderer, MouseInput mouseInput) throws Exception {
        while (!glfwWindowShouldClose(window)) {
            mouseInput.input();
            renderer.doRender();

            //ВЫПОЛНЯЕТСЯ ОТРИСОВКА
            glfwSwapBuffers(window); // swap the color buffers
           glfwPollEvents();
        }
    }
}
