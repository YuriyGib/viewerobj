package enums;

public enum ConstPath {
        FILE_MTL("/models/orel/eagle_2.MTL"),
        FILE_OBJ("/models/orel/eagle_2.obj"),
        FILE_OBJ_WOLF("/models/wolf/WOLF.obj"),
        FILE_MTL_WOLF("/models/wolf/WOLF.MTL"),
        FILE_TEXTURE("/textures/images/"),
        FILE_OBJ1("/models/cube.obj"),
        FILE_HOUSE("src/main/resources/models/house/house.obj"),
        FILE_PATH_HOUSE("/textures/images"),
        FILE_SKYBOX("src/main/resources/models/skybox.obj"),
        FILE_PATH_SKYBOX("/textures/skybox.png");

        public String filePath;

        ConstPath(String s) {
            filePath = s;
        }
}
