package engine;

import lombok.Getter;
import lombok.Setter;

import org.joml.Intersectionf;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;


import java.nio.FloatBuffer;


@Getter
@Setter
public class Rectangle {

    private static final float z = 1;
    private static final float k = (float) 480 / 640;


    public static FloatBuffer getRectangle(Vector3f dx, Vector3f dy) {
        int[] viewport = new int[4];
        viewport[2] = 640;
        viewport[3] = 480;
        Float prevX = -dx.x * dx.z;
        Float prevY = dx.y * dx.z * k;
        Float curX = -dy.x * dx.z;
        Float curY = dy.y * dx.z * k;



        Vector3f a1=new Vector3f();
        Vector3f a2 = new Vector3f();
        new Matrix4f().unprojectRay(prevX,prevY,viewport,a1,a2);



        Vector3f a3=new Vector3f();
        Vector3f a4 = new Vector3f();

        new Matrix4f().unprojectRay(prevX,curY,viewport,a3,a4);
        Vector3f a5=new Vector3f();
        Vector3f a6 = new Vector3f();
        new Matrix4f().unprojectRay(curX,curY,viewport,a5,a6);

        Vector3f a7=new Vector3f();
        Vector3f a8 = new Vector3f();
        new Matrix4f().unprojectRay(curX,prevY,viewport,a7,a8);




        return BufferUtils.createFloatBuffer(3 * 16 * Float.BYTES).put(new float[]{
                prevX, prevY, z, prevX, curY, z,
                prevX, curY, z, curX, curY, z,
                curX, curY, z, curX, prevY, z,
                prevX, prevY, z, curX, prevY, z,

                prevX,prevY, z, a2.x, a2.y, -1000,
                prevX,curY, z, a4.x, a4.y, -1000,
                curX,curY, z, a6.x, a6.y, -1000,
                curX,prevY, z, a8.x, a8.y, -1000,

        }).rewind();

    }

}