package engine;

import lombok.Getter;
import lombok.Setter;
import model.*;
import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class SceneManyTexture {
    private static final int vertexLocation = 0;
    private static final int textureLocation = 2;
    private static final int normalLocation = 1;

    @Getter
    @Setter
    private Map<String,Material> materials;

    @Getter
    private List<ArrayIdTriangle> vbao = new ArrayList<>();

    //если много текстур
    //здесь создаются буферы под obj составляющие и передаются в opengl
    public SceneManyTexture(float[] positions, float[] textCoords, float[] normals, List<int[]> indicesArr,List<ObjTexture> objTextures) throws Exception {
        FloatBuffer posBuffer = null;
        FloatBuffer textCoordsBuffer = null;
        FloatBuffer vecNormalsBuffer = null;
        IntBuffer indicesBuffer = null;
        try {
            int i = 0;
            for (int[] indices : indicesArr) {
                int vId = glGenVertexArrays();
                glBindVertexArray(vId);
                vbao.add(new ArrayIdTriangle(vId, indices.length,objTextures.get(i)));
                int vertexBuffer = glGenBuffers();
                posBuffer = MemoryUtil.memAllocFloat(positions.length);
                posBuffer.put(positions).flip();
                glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
                glBufferData(GL_ARRAY_BUFFER, posBuffer, GL_STATIC_DRAW);
                glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, false, 0, 0);

                // Texture coordinates VBO
                int textureBuffer = glGenBuffers();
                textCoordsBuffer = MemoryUtil.memAllocFloat(textCoords.length);
                textCoordsBuffer.put(textCoords).flip();
                glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
                glBufferData(GL_ARRAY_BUFFER, textCoordsBuffer, GL_STATIC_DRAW);
                glVertexAttribPointer(textureLocation, 2, GL_FLOAT, false, 0, 0);

                // Vertex normals VBO
                int normalBuffer = glGenBuffers();
                vecNormalsBuffer = MemoryUtil.memAllocFloat(normals.length);
                vecNormalsBuffer.put(normals).flip();
                glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
                glBufferData(GL_ARRAY_BUFFER, vecNormalsBuffer, GL_STATIC_DRAW);
                glVertexAttribPointer(normalLocation, 3, GL_FLOAT, false, 0, 0);

                // Index VBO
                int indexBuffer = glGenBuffers();
                indicesBuffer = MemoryUtil.memAllocInt(indices.length);
                indicesBuffer.put(indices).flip();
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW);
                i++;
            }
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        } finally {
            if (posBuffer != null) {
                MemoryUtil.memFree(posBuffer);
            }
            if (textCoordsBuffer != null) {
                MemoryUtil.memFree(textCoordsBuffer);
            }
            if (vecNormalsBuffer != null) {
                MemoryUtil.memFree(vecNormalsBuffer);
            }
            if (indicesBuffer != null) {
                MemoryUtil.memFree(indicesBuffer);
            }
        }
    }
}