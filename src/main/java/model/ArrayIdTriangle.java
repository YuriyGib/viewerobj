package model;

import lombok.Getter;

@Getter
public class ArrayIdTriangle {
    private int id;
    private int size;
    private ObjTexture objTexture;

    public ArrayIdTriangle(int id, int size, ObjTexture objTexture) {
        this.id = id;
        this.size = size;
        this.objTexture = objTexture;
    }
}
