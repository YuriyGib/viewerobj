package model;

import engine.SceneOneTexture;
import lombok.Getter;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;
import util.StaticMeshesLoader;
import util.Texture;

import java.nio.FloatBuffer;

public class SkyBox {

    public Vector3f cameraPosition = new Vector3f(5, 1, 5);
    public Vector3f modelPosition = new Vector3f(0f, 0, 0);
    public Vector3f modelRotation = new Vector3f(0, 0, 0);
    public Vector3f modelScale = new Vector3f(20f, 20f, 20f);

    public FloatBuffer pMatrix = BufferUtils.createFloatBuffer(16);

    {
        new Matrix4f().perspective((float) Math.toRadians(45.0f), 640f / 480f, 0.01f, 100.0f)
                .get(pMatrix);
    }

    @Getter
    SceneOneTexture skyBoxMesh;

    public SkyBox(String objModel, String textureFile) throws Exception {

        skyBoxMesh = StaticMeshesLoader.load(objModel, "")[0];
        Texture skyBoxtexture = new Texture(textureFile);
        skyBoxMesh.setMaterial(new Material(skyBoxtexture, 0.0f));

    }

    public SkyBox(String objModel, Vector4f colour) throws Exception {
        skyBoxMesh = StaticMeshesLoader.load(objModel, "", 0)[0];
        Material material = new Material(colour, 0);
        skyBoxMesh.setMaterial(material);
    }

}
