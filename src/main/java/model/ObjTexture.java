package model;

import lombok.Getter;
import util.Texture;

@Getter
public class ObjTexture {
    private String nameTexture;
    private String texturePath;

    public ObjTexture(String nameTexture, String texturePath) {
        this.nameTexture = nameTexture;
        this.texturePath = texturePath;
    }
}
