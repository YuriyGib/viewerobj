package util;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;


public class Geometry {

    public static final FloatBuffer axis = BufferUtils.createFloatBuffer(3 * 6 * Float.BYTES).put(new float[]{
            // x
            0, 0, 0, 100, 0, 0,
            // y
            0, 0, 0, 0, 100, 0,
            // z
            0, 0, 0, 0, 0, 100
    });
    private static int q=2;
    private static int z=0;
    private static float k = 1 / 25f;
    public static double pikx = 0;
    public static double piky = 0;


    public static FloatBuffer getRectangle(double x, double y) {
        pikx = x;
        piky = y;
        return BufferUtils.createFloatBuffer(3 * 8 * Float.BYTES).put(new float[]{
                // x
                (float) (- x), (float) (y), z+q, (float) (x), (float) (y), z,//12
                // y
                (float) (- x), (float) (y),  z+q, (float) (-x), (float) (-y),  z+q,//13
                // z
                (float) ( x), (float) (y),  z, (float) (x), (float) (-y),  z,//24
                (float) (- x), (float) (-y),  z+q, (float) (x), (float) (-y), z,//34
        }).rewind();
    }

    static {
        axis.rewind();
        //rectangle.rewind();
    }
}