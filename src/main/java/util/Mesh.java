package util;

import lombok.Getter;

import java.util.List;

@Getter
public class Mesh {
    private float[] posArr;
    private float[] textCoordArr;
    private float[] normArr;
    private List<int[]> indicesArr;

    public Mesh(float[] posArr, float[] textCoordArr, float[] normArr, List<int[]> indicesArr) {
        this.indicesArr = indicesArr;
        this.posArr = posArr;
        this.textCoordArr = textCoordArr;
        this.normArr = normArr;
    }
}
