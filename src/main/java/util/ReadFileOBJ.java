package util;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import org.joml.Vector2f;
import org.joml.Vector3f;


public class ReadFileOBJ {

    public static Mesh loadMesh(String fileName) throws Exception {
        List<Vector3f> vertices = new ArrayList<>();
        List<Vector2f> textures = new ArrayList<>();
        List<Vector3f> normals = new ArrayList<>();
        List<String> nameMaterialsG = new ArrayList<>();
        List<String> nameMaterialsUSEMTL = new ArrayList<>();
        List<Face> faces = new ArrayList<>();
        Files.lines(Paths.get(ReadFileOBJ.class.getResource(fileName).toURI()), StandardCharsets.UTF_8)
                .forEachOrdered(s -> {
                    String[] tokens = s.split("\\s+");
                    switch (tokens[0]) {
                        case "v":
                            // Geometric vertex
                            Vector3f vec3f = new Vector3f(
                                    Float.parseFloat(tokens[1]),
                                    Float.parseFloat(tokens[2]),
                                    Float.parseFloat(tokens[3]));
                            vertices.add(vec3f);
                            break;
                        case "vt":
                            // Текстурные координаты
                            Vector2f vec2f = new Vector2f(
                                    Float.parseFloat(tokens[1]),
                                    Float.parseFloat(tokens[2]));
                            textures.add(vec2f);
                            break;
                        case "g":
                            if (tokens.length > 1) {
                                nameMaterialsG.add(tokens[1]);
                            }
                            break;
                        case "usemtl":
                            if (tokens.length > 1) {
                                nameMaterialsUSEMTL.add(tokens[1]);
                            }
                            break;
                        case "vn":
                            // Нормали вершин
                            Vector3f vec3fNorm = new Vector3f(
                                    Float.parseFloat(tokens[1]),
                                    Float.parseFloat(tokens[2]),
                                    Float.parseFloat(tokens[3]));
                            normals.add(vec3fNorm);
                            break;
                        case "f":
                            String nameTexture = null;
                            if (!nameMaterialsG.isEmpty()) {
                                nameTexture = nameMaterialsG.get(nameMaterialsG.size() - 1);
                            }
                            if (!nameMaterialsUSEMTL.isEmpty()) {
                                nameTexture = nameMaterialsUSEMTL.get(nameMaterialsUSEMTL.size() - 1);
                            }
                            Face face = new Face(tokens[1], tokens[2], tokens[3], nameTexture);
                            faces.add(face);
                            break;
                        default:
                            // Ignore other lines
                            break;
                    }
                });
        return reorderLists(vertices, textures, normals, faces);
    }

    private static Mesh reorderLists(List<Vector3f> posList, List<Vector2f> textCoordList,
                                     List<Vector3f> normList, List<Face> facesList) {


        // Create position array in the order it has been declared
        float[] posArr = new float[posList.size() * 3];
        int i = 0;
        for (Vector3f pos : posList) {
            posArr[i * 3] = pos.x;
            posArr[i * 3 + 1] = pos.y;
            posArr[i * 3 + 2] = pos.z;
            i++;
        }
        float[] textCoordArr = new float[posList.size() * 2];
        float[] normArr = new float[posList.size() * 3];

        List<List<Face>> listTextureFromFace = getTextureListFromFace(facesList);
        List<List<Integer>> listIndices = new ArrayList<>();
        for (List<Face> faces : listTextureFromFace) {
            List<Integer> indices = new ArrayList();
            for (Face face : faces) {
                IdxGroup[] faceVertexIndices = face.getIdxGroups();
                for (IdxGroup indValue : faceVertexIndices) {
                    processFaceVertex(indValue, textCoordList, normList,
                            indices, textCoordArr, normArr);
                }
            }
            listIndices.add(indices);
        }
        List<int[]> indicesArr = new ArrayList<>();

        for (List<Integer> indices : listIndices) {
            indicesArr.add(indices.stream().mapToInt((Integer v) -> v).toArray());
        }
        return new Mesh(posArr, textCoordArr, normArr, indicesArr);
    }

    private static void processFaceVertex(IdxGroup indices, List<Vector2f> textCoordList,
                                          List<Vector3f> normList, List<Integer> indicesList,
                                          float[] texCoordArr, float[] normArr) {

        // Set index for vertex coordinates
        int posIndex = indices.idxPos;
        indicesList.add(posIndex);

        // Reorder texture coordinates
        if (indices.idxTextCoord >= 0) {
            Vector2f textCoord = textCoordList.get(indices.idxTextCoord);
            texCoordArr[posIndex * 2] = textCoord.x;
            texCoordArr[posIndex * 2 + 1] = 1 - textCoord.y;
        }
        if (indices.idxVecNormal >= 0) {
            Vector3f vecNorm = normList.get(indices.idxVecNormal);
            normArr[posIndex * 3] = vecNorm.x;
            normArr[posIndex * 3 + 1] = vecNorm.y;
            normArr[posIndex * 3 + 2] = vecNorm.z;
        }
    }

    //считывает все face, которые принадлежат одной текстуре
    private static List<List<Face>> getTextureListFromFace(List<Face> faces) {
        String nameTexture = faces.get(0).getNameTexture();
        List<List<Face>> textureListFromFace = new ArrayList<>();
        if (nameTexture != null) {
            List<Face> textureFromFace = new ArrayList<>();
            for (Face face : faces) {
                if (face.getNameTexture() != null && face.getNameTexture().equals(nameTexture)) {
                    textureFromFace.add(face);
                } else {
                    textureListFromFace.add(textureFromFace);
                    nameTexture = face.getNameTexture();
                    textureFromFace = new ArrayList<>();
                    textureFromFace.add(face);
                }
            }
            if (!textureFromFace.isEmpty())
                textureListFromFace.add(textureFromFace);
        } else {
            textureListFromFace.add(faces);
        }
        return textureListFromFace;
    }

    protected static class Face {

        @Getter
        private IdxGroup[] idxGroups = new IdxGroup[3];
        @Getter
        private String nameTexture;


        Face(String v1, String v2, String v3, String nameTexture) {
            idxGroups = new IdxGroup[3];
            // Parse the lines
            idxGroups[0] = parseLine(v1);
            idxGroups[1] = parseLine(v2);
            idxGroups[2] = parseLine(v3);
            this.nameTexture = nameTexture;
        }

        private IdxGroup parseLine(String line) {
            IdxGroup idxGroup = new IdxGroup();

            String[] lineTokens = line.split("/");
            int length = lineTokens.length;
            idxGroup.idxPos = Integer.parseInt(lineTokens[0]) - 1;
            if (length > 1) {
                // It can be empty if the obj does not define text coords
                String textCoord = lineTokens[1];
                idxGroup.idxTextCoord = textCoord.length() > 0 ? Integer.parseInt(textCoord) - 1 : IdxGroup.NO_VALUE;
                if (length > 2) {
                    idxGroup.idxVecNormal = Integer.parseInt(lineTokens[2]) - 1;
                }
            }

            return idxGroup;
        }
    }

    static class IdxGroup {

        static final int NO_VALUE = -1;

        int idxPos;

        int idxTextCoord;

        int idxVecNormal;

        IdxGroup() {
            idxPos = NO_VALUE;
            idxTextCoord = NO_VALUE;
            idxVecNormal = NO_VALUE;
        }
    }
}
