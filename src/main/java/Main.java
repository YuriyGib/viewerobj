import com.mokiat.data.front.parser.MTLLibrary;
import com.mokiat.data.front.parser.MTLMaterial;
import engine.SceneManyTexture;
import engine.SceneOneTexture;
import enums.ConstPath;
import model.Material;
import model.ObjTexture;
import model.SkyBox;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFWErrorCallback;
import util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.*;

public class Main {
    enum WHAT_MOVE {
        MODEL, CAMERA, LIGHT
    }

    static class ControlState {
        WHAT_MOVE whatMove = WHAT_MOVE.MODEL;
    }

    public static void main(String[] args) throws Exception {
        MouseInput mouseInput = new MouseInput();
        MTLLibrary mtlLibrary = Util.readMTL(ConstPath.FILE_MTL.filePath);//ОРЕЛ
        //MTLLibrary mtlLibrary = Util.readMTL(ConstPath.FILE_MTL_WOLF.filePath);//ВОЛК
        GLFWErrorCallback.createPrint(System.err).set();
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        Window wnd = new Window(640, 480, "Лабораторная работа № 1 по ТИ");
        mouseInput.init(wnd);
        List<ObjTexture> objTextureList = new ArrayList<>();
        if (mtlLibrary != null) {
            for (MTLMaterial mtlMaterial : mtlLibrary.getMaterials()) {
                if (mtlMaterial.getDiffuseTexture() != null) {
                    objTextureList.add(new ObjTexture(mtlMaterial.getName(), mtlMaterial.getDiffuseTexture().replace("\\", "/").replace("//", "/")));
                }else{
                    objTextureList.add(new ObjTexture(mtlMaterial.getName(),null));
                }
            }
        }
        ControlState cs = new ControlState();
        Renderer render = new Renderer();

        SkyBox skyBox = new SkyBox(ConstPath.FILE_SKYBOX.filePath, ConstPath.FILE_PATH_SKYBOX.filePath);

        //если несколько текстур
        ///ОРЕЛ
        Mesh mesh = ReadFileOBJ.loadMesh(ConstPath.FILE_OBJ.filePath);
        Map<String,Material> materials = Util.getMaterials(objTextureList);
        SceneManyTexture sceneManyTexture = new SceneManyTexture(mesh.getPosArr(),mesh.getTextCoordArr(),mesh.getNormArr(),mesh.getIndicesArr(),objTextureList);
        sceneManyTexture.setMaterials(materials);


        ///КУБ
          //Mesh mesh = ReadFileOBJ.loadMesh(ConstPath.FILE_OBJ1.filePath);
         // SceneManyTexture sceneManyTexture = new SceneManyTexture(mesh.getPosArr(),mesh.getTextCoordArr(),mesh.getNormArr(),mesh.getIndicesArr(),objTextureList);
//        Texture texture = new Texture(ConstPath.FILE_TEXTURE.filePath + "cat-texture.jpg", true);
//        Map<String,Material> materials = new HashMap<>();
//        materials.put("fdf",new Material(texture));
//        sceneManyTexture.setMaterials(materials);

        ///ВОЛК
//        Mesh mesh = ReadFileOBJ.loadMesh(ConstPath.FILE_OBJ_WOLF.filePath);
//        Map<String,Material> materials = Util.getMaterials(objTextureList);
//        SceneManyTexture sceneManyTexture = new SceneManyTexture(mesh.getPosArr(),mesh.getTextCoordArr(),mesh.getNormArr(),mesh.getIndicesArr(),objTextureList);
//        sceneManyTexture.setMaterials(materials);

        //ДОМИК
//        SceneOneTexture[] sceneRenderers = util.StaticMeshesLoader.load(ConstPath.FILE_HOUSE.filePath, ConstPath.FILE_PATH_HOUSE.filePath);
//        render.modelScale.sub(4.15f, 4.15f, 4.15f);

        wnd.setKeyListener((key, scancode, action) -> {
            switch (key) {
                case GLFW_KEY_1:
                    System.out.println("move model");
                    cs.whatMove = WHAT_MOVE.MODEL;
                    break;
                case GLFW_KEY_2:
                    System.out.println("move camera");
                    cs.whatMove = WHAT_MOVE.CAMERA;
                    break;
                case GLFW_KEY_3:
                    System.out.println("move light");
                    cs.whatMove = WHAT_MOVE.LIGHT;
                    break;
            }

            Vector3f whatToMove = null;
            switch (cs.whatMove) {
                case MODEL:
                    whatToMove = render.modelPosition;
                    break;
                case CAMERA:
                    whatToMove = render.cameraPosition;
                    break;
                case LIGHT:
                    whatToMove = render.lightPosition;
                    break;

            }

            float step = 0.1f;

            switch (key) {
                case GLFW_KEY_W:
                    whatToMove.add(0, step, 0);
                    break;
                case GLFW_KEY_S:
                    whatToMove.sub(0, step, 0);
                    break;
                case GLFW_KEY_A:
                    whatToMove.sub(step, 0, 0);
                    break;
                case GLFW_KEY_D:
                    whatToMove.add(step, 0, 0);
                    break;
                case GLFW_KEY_Q:
                    whatToMove.sub(0, 0, step);
                    break;
                case GLFW_KEY_E:
                    whatToMove.add(0, 0, step);
                    break;
                case GLFW_MOUSE_BUTTON_2:
                    break;
                case GLFW_KEY_KP_3:
                    render.modelRotation.add(0, 0, step);
                    break;
                case GLFW_KEY_KP_1:
                    render.modelRotation.sub(0, 0, step);
                    break;
                case GLFW_KEY_KP_8:
                    render.modelRotation.add(0, step, 0);
                    break;
                case GLFW_KEY_KP_2:
                    render.modelRotation.sub(0, step, 0);
                    break;
                case GLFW_KEY_KP_6:
                    render.modelRotation.add(step, 0, 0);
                    break;
                case GLFW_KEY_KP_4:
                    render.modelRotation.sub(step, 0, 0);
                    break;
                case GLFW_KEY_KP_ADD:
                    render.modelScale.add(step, step, step);
                    break;
                case GLFW_KEY_KP_SUBTRACT:
                    render.modelScale.sub(step, step, step);
                    break;

            }
        });
        wnd.render(() -> {
            render.modelRotation.add(render.modelRotationSpeed);

            render.render(sceneManyTexture,skyBox,mouseInput);
            //render.render(sceneRenderers,skyBox,mouseInput);
        },mouseInput);
        glfwTerminate();
    }
}