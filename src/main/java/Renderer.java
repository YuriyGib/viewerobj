import engine.Rectangle;
import engine.SceneManyTexture;
import engine.SceneOneTexture;

import model.ArrayIdTriangle;
import model.Material;
import model.SkyBox;
import org.apache.commons.io.IOUtils;
import org.joml.*;
import org.lwjgl.BufferUtils;
import util.Texture;
import util.Util;

import java.lang.Math;
import java.nio.FloatBuffer;
import java.util.Map;
import java.util.Optional;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_POINTS;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class Renderer {
    private static Boolean flag = false;
    private static final int vertexLocation = 0;
    private static final int textureLocation = 2;
    private static final int normalLocation = 1;
    private static final float MOUSE_SENSITIVITY = 0.02f;
    private final int vaoRectangle;

    private final int rectangleProgram;
    private final int rectangleBuffer;

    Vector3f lightPosition = new Vector3f(0, 0, 2);
    Vector3f modelPosition = new Vector3f(0.2f, 0, 0);
    //   Vector3f cameraPosition = new Vector3f(3.6999989f, -7.4505806e-8f, 3.8999984f);
    Vector3f cameraPosition = new Vector3f(0, 0, 5);
    private FloatBuffer lightPoint = BufferUtils.createFloatBuffer(3);

    Vector3f modelRotation = new Vector3f(0, 0, 0);
    Vector3f modelScale = new Vector3f(4.2f, 4.2f, 4.2f);
    Vector3f modelRotationSpeed = new Vector3f(0.0f, 0.0f, 0.0f);
    Matrix4f p = new Matrix4f().perspective((float) Math.toRadians(45.0f), 640f / 480f, 0.01f, 100.0f);
    private FloatBuffer pMatrix = BufferUtils.createFloatBuffer(16);

    {
        p
                .get(pMatrix);
    }

    private int axisProgram;
    private int shaderProgram;
    private int skyBoxProgram;
    private int vaoLines;
    private int vaoPoints;
    private int pointsBuffer;


    Renderer() throws Exception {
        glEnable(GL_DEPTH_TEST);
        glClearColor(0.04f, 1.0f, 0.2f, 1.0f);
        glPointSize(20);//размер точки


        String fragmentSource = IOUtils.toString(getClass().getResourceAsStream("shaders/shader.frag"));
        String vertexSource = IOUtils.toString(getClass().getResourceAsStream("shaders/shader.vert"));
        shaderProgram = Util.createShaderProgram(vertexSource, fragmentSource);

        String simpleVertexSource = IOUtils.toString(getClass().getResourceAsStream("shaders/simple.vert"));
        String simpleFragmentSource = IOUtils.toString(getClass().getResourceAsStream("shaders/simple.frag"));
        axisProgram = Util.createShaderProgram(simpleVertexSource, simpleFragmentSource);

        String skyBoxVertexSource = IOUtils.toString(getClass().getResourceAsStream("shaders/sb.vert"));
        String skyBoxFragmentSource = IOUtils.toString(getClass().getResourceAsStream("shaders/sb.frag"));
        skyBoxProgram = Util.createShaderProgram(skyBoxVertexSource, skyBoxFragmentSource);

        String rectangleVertexSource = IOUtils.toString(getClass().getResourceAsStream("shaders/rectangle.vert"));
        String rectangleFragmentSource = IOUtils.toString(getClass().getResourceAsStream("shaders/rectangle.frag"));
        rectangleProgram = Util.createShaderProgram(rectangleVertexSource, rectangleFragmentSource);

//        int axisBuffer = glGenBuffers();
//        glBindBuffer(GL_ARRAY_BUFFER, axisBuffer);
//        GL15.glBufferData(GL_ARRAY_BUFFER, Geometry.axis, GL_STATIC_DRAW);
        vaoLines = glGenVertexArrays();//вао под систему координат заполняется в момент рендеринга статически
        glBindVertexArray(vaoLines);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);


        //заполняется в момент рендеринга
        pointsBuffer = glGenBuffers();//квадрат под источник света(точка)
        vaoPoints = glGenVertexArrays();//вао поинтс под квадрат с источником света

        vaoRectangle = glGenVertexArrays();//вао под прямоугольник
        glBindVertexArray(vaoRectangle);
        rectangleBuffer = glGenBuffers();//квадрат заполняется в момент рендеринга
        //glBindBuffer(GL_ARRAY_BUFFER, rectangleBuffer);
        //GL15.glBufferData(GL_ARRAY_BUFFER, Geometry.rectangle, GL_STATIC_DRAW);
        //glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
    }

private static Vector3f fb =new Vector3f();
    //начальная подготовка к рендерингу obj модели
    private void initModelRender(FloatBuffer vMatrix) {
        FloatBuffer mMatrix = BufferUtils.createFloatBuffer(16);
        new Matrix4f().translate(modelPosition)
                .rotateX(modelRotation.x)
                .rotateY(modelRotation.y)
                .rotateZ(modelRotation.z)
                .scale(modelScale)
                .get(mMatrix);
        glUseProgram(shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "P"), false, pMatrix);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "V"), false, vMatrix);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "M"), false, mMatrix);
        glUniform3f(glGetUniformLocation(shaderProgram, "light_worldspace"), lightPosition.x, lightPosition.y, lightPosition.z);
    }

    private static Optional<Vector3f> startPointMouse = Optional.empty();
    private static Optional<Vector3f> endPointMouse = Optional.empty();

    private void renderSkyBox(SkyBox skyBox) {
        glUseProgram(skyBoxProgram);
        FloatBuffer mMatrix = BufferUtils.createFloatBuffer(16);
        new Matrix4f().translate(skyBox.modelPosition)
                .rotateX(skyBox.modelRotation.x)
                .rotateY(skyBox.modelRotation.y)
                .rotateZ(skyBox.modelRotation.z)
                .scale(skyBox.modelScale)
                .get(mMatrix);
        FloatBuffer vMatrix = BufferUtils.createFloatBuffer(16);
        new Matrix4f()
                .lookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z,
                        0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f)
                .m30(0)
                .m31(0)
                .m32(0)
                .get(vMatrix);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "P"), false, skyBox.pMatrix);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "V"), false, vMatrix);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "M"), false, mMatrix);
        SceneOneTexture render = skyBox.getSkyBoxMesh();
        Texture texture = render.getMaterial() != null ? render.getMaterial().getTexture() : null;
        if (texture != null) {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture.getId());
        }
        glBindVertexArray(render.getVaoTriangles());
        glEnableVertexAttribArray(vertexLocation);
        glEnableVertexAttribArray(textureLocation);
        glEnableVertexAttribArray(normalLocation);
        glDrawElements(GL_TRIANGLES, render.getVertexCount(), GL_UNSIGNED_INT, 0);
    }
    private static Vector3f fa= new Vector3f();
    private static double hy = 0;
    private static double hx = 0;

    //рисование прямоугольника
    private void renderRectangle(FloatBuffer vMatrix, MouseInput mouseInput) {
        Matrix4f p = new Matrix4f().set(pMatrix);
        Matrix4f v = new Matrix4f().set(vMatrix);

        Matrix4f q = p.mul(v);

        FloatBuffer qF = BufferUtils.createFloatBuffer(16);
        q.get(qF);




        //найдем обратную матрицу
        Matrix4f qInv = new Matrix4f();
        q.invert(qInv);

        FloatBuffer qIF = BufferUtils.createFloatBuffer(16);
        qInv.get(qIF);

        //-2,3,2  -2 1 2


   q=new Matrix4f();

       // Matrix4f fda = q.mul(qInv);
        Vector2d mouse = mouseInput.getCurrentPos();
        Vector3f origin = new Vector3f();
        Vector3f dir = new Vector3f();
        Vector3f point = new Vector3f();
        Vector3f normal = new Vector3f(0, 1, 0);
        int[] viewport = new int[4];
        viewport[2] = 640;
        viewport[3] = 480;
        qInv.unprojectRay((float) mouse.x, 480 - (float) mouse.y, viewport, origin, dir);
        float t = Intersectionf.intersectRayPlane(origin, dir, point, normal, 1E-6f);
        Vector4f a = new Vector4f((float) mouse.x, (float) mouse.y, -1.0f,1.0f);

Vector3f di1r = new Vector3f();
Vector3f e = new Vector3f();
       Matrix4f s= q.unprojectRay((float)mouse.x,(float)mouse.y,viewport,e,dir);

        Vector3f b = new Vector3f((float) mouse.x, (float) mouse.y, 1.0f);


        glUseProgram(rectangleProgram);
        glUniformMatrix4fv(glGetUniformLocation(rectangleProgram, "Q"), false, qF);
        //glUniformMatrix4fv(glGetUniformLocation(rectangleProgram, "QInv"), false, r);
        glBindVertexArray(vaoRectangle);
        glBindBuffer(GL_ARRAY_BUFFER, rectangleBuffer);//pointbuffer-камера, точка помещаем ее в буффер


        if (mouseInput.isLeftButtonPressed()) {
            if (!startPointMouse.isPresent()) {

                Vector3f dest = new Vector3f();
                q.unproject((float)mouse.x,(float)mouse.y,-1.0f,viewport,dest);
                startPointMouse = Optional.ofNullable(dest);
            }
            Vector3f dest = new Vector3f();
            q.unproject((float)mouse.x,(float)mouse.y,1.0f,viewport,dest);
            endPointMouse =  Optional.ofNullable(dest);

        } else {


            if (startPointMouse.isPresent() && endPointMouse.isPresent()) {
                q.unprojectRay((float)mouse.x,(float)mouse.y,viewport,fa,fb);
             //   endPointMouse.ifPresent(s -> System.out.println(" endPointMouse x " + s.x + " y " + s.y));
                glBufferData(GL_ARRAY_BUFFER, Rectangle.getRectangle(startPointMouse.get(), endPointMouse.get()), GL_STATIC_DRAW);
                glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
                glEnableVertexAttribArray(0);
                startPointMouse = Optional.empty();
                endPointMouse = Optional.empty();
            }
        }

        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 1, 0.6f, 0);//верхняя
        glDrawArrays(GL_LINES, 0, 2);
        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 0, 1, 0);
        glDrawArrays(GL_LINES, 2, 2);
        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 0, 0, 1);
        glDrawArrays(GL_LINES, 4, 2);
        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 0, 0, 1);//синий
        glDrawArrays(GL_LINES, 6, 2);

        glDrawArrays(GL_LINES, 8, 2);
        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 0, 1, 0);
        glDrawArrays(GL_LINES, 10, 2);
        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 0, 0, 1);
        glDrawArrays(GL_LINES, 12, 2);
        glUniform3f(glGetUniformLocation(rectangleProgram, "c"), 0, 0, 1);//синий
        glDrawArrays(GL_LINES, 14, 2);


    }

    //рендеринг координатной прямой
    private void renderAxis(FloatBuffer vMatrix) {
        glUseProgram(axisProgram);
        glUniformMatrix4fv(glGetUniformLocation(axisProgram, "P"), false, pMatrix);
        glUniformMatrix4fv(glGetUniformLocation(axisProgram, "V"), false, vMatrix);
        //    glBindVertexArray(vaoLines);
        //рисует ось координатную
//        glEnableVertexAttribArray(0);
//        glUniform3f(glGetUniformLocation(axisProgram, "c"), 1, 0, 0);
//        glDrawArrays(GL_LINES, 0, 2);
//        glUniform3f(glGetUniformLocation(axisProgram, "c"), 0, 1, 0);
//        glDrawArrays(GL_LINES, 2, 2);
//        glUniform3f(glGetUniformLocation(axisProgram, "c"), 0, 0, 1);
//        glDrawArrays(GL_LINES, 4, 2);
    }

    private void renderLight() {
        lightPosition.get(lightPoint);
        glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);//pointbuffer-камера, точка помещаем ее в буффер
        glBufferData(GL_ARRAY_BUFFER, lightPoint, GL_STATIC_DRAW);//говорим, что эта точка имеет точно такие же координаты как и источник освещения
        glBindVertexArray(vaoPoints);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(0);
        glDrawArrays(GL_POINTS, 0, 1);//нарисовали камеру
    }

    //рендеринг для obj файла с одной текстурой
    public void render(SceneOneTexture[] renderArr, SkyBox skyBox, MouseInput mouseInput) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
        Vector2f rotVec = mouseInput.getDisplVec();
        if (mouseInput.isRightButtonPressed()) {
            modelRotation.sub(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
        }
        if (mouseInput.isLeftButtonPressed()) {
            modelRotation.add(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
        }
        FloatBuffer vMatrix = BufferUtils.createFloatBuffer(16);
        new Matrix4f()
                .lookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z,
                        0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f).get(vMatrix);
        renderSkyBox(skyBox);
        renderAxis(vMatrix);

        renderLight();
        initModelRender(vMatrix);

        for (SceneOneTexture render : renderArr) {
            Optional<Texture> texture = Optional.ofNullable(render.getMaterial().getTexture());
            if (texture.isPresent()) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, texture.get().getId());
            }
            glBindVertexArray(render.getVaoTriangles());
            glEnableVertexAttribArray(vertexLocation);
            glEnableVertexAttribArray(textureLocation);
            glEnableVertexAttribArray(normalLocation);
            glDrawElements(GL_TRIANGLES, render.getVertexCount(), GL_UNSIGNED_INT, 0);
        }
    }

    //рендеринг для obj файла с несколькими текстурами
    void render(SceneManyTexture sceneManyTexture, SkyBox skyBox, MouseInput mouseInput) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
        Vector2f rotVec = mouseInput.getDisplVec();
        if (mouseInput.isRightButtonPressed()) {
            modelRotation.sub(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
        }
//        if (mouseInput.isLeftButtonPressed()) {
//            modelRotation.add(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);
//        }
        FloatBuffer vMatrix = BufferUtils.createFloatBuffer(16);
        new Matrix4f()
                .lookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z,
                        0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f).get(vMatrix);
        //System.out.println(" X = "+cameraPosition.x+ " y " + cameraPosition.y +" z = "+ cameraPosition.z);
        renderSkyBox(skyBox);
        renderRectangle(vMatrix, mouseInput);
        renderAxis(vMatrix);
        renderLight();
        initModelRender(vMatrix);
        for (ArrayIdTriangle aVbao : sceneManyTexture.getVbao()) {
            Map<String, Material> materials = sceneManyTexture.getMaterials();
            String nameTexture = aVbao.getObjTexture().getNameTexture();
            Optional<Texture> texture = Optional.ofNullable(materials.get(nameTexture).getTexture());
            if (texture.isPresent()) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, texture.get().getId());
            }
            glBindVertexArray(aVbao.getId());
            glEnableVertexAttribArray(vertexLocation);
            glEnableVertexAttribArray(textureLocation);
            glEnableVertexAttribArray(normalLocation);
            glDrawElements(GL_TRIANGLES, aVbao.getSize(), GL_UNSIGNED_INT, 0);

        }


    }
}
