#version 330

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out vec2 outTexCoord;

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

void main()
{
    outTexCoord = texCoord;
       vec3 vertex_modelspace = (M * vec4(vertexPos.xyz, 1.0)).xyz;
        gl_Position = P * V * vec4(vertex_modelspace.xyz, 1.0);
       vec3 normal_modelspace = (M * vec4(normal.xyz, 1.0)).xyz;
      	vec3 EyeDirection_cameraspace = vec3(0,0,0) - (V * M * vec4(vertexPos,1)).xyz;
}