#version 330

layout(location = 0) in vec3 vertexPos;

uniform mat4 Q;

void main() {

     gl_Position = Q * vec4(vertexPos.xyz, 1.0);

}