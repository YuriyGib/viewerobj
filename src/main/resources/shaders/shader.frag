#version 330 core

in vec3 normal_modelspace;
in vec3 vertex_modelspace;
in vec2 TexCoord;

out vec4 color;

uniform vec3 light_worldspace;

uniform sampler2D ourTexture;

void main() {
  vec3 n = normalize(normal_modelspace);
  vec3 l = normalize(light_worldspace - vertex_modelspace);
  float cosTheta = clamp( dot( n, l), 0,1 );
  float ambient = 0.05;
  color = texture(ourTexture, TexCoord)*(cosTheta + ambient);
}

